class ArticlesController < ApplicationController
  http_basic_authenticate_with name: 'welcome', password: 'secret', except: [:index, :show]
  before_action :find_article, except: [:new, :create, :index]
    
  def index
    @articles = Article.all
  end
  
  def new
    @article = Article.new
  end
  
  def create
    allowed_params = params.require(:article).permit(:title, :description)
    @article = Article.create(allowed_params)
  
    if @article.errors.any?
      render :new
    else
      redirect_to articles_path
    end    
  end
  
  def edit
  end
  
  def update
    @article.update_attributes(params.require(:article).permit(:title, :description))
    
    redirect_to articles_path
  end
  
  def show
  end
  
  def destroy
    @article.destroy
  
    redirect_to articles_path, notice: "Delete success"
  end
  
  
  private 
  
  def find_article
    @article = Article.find(params[:id])
  end
  
end
